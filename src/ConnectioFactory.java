import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectioFactory {
	
	public Connection recuperarConexao() throws SQLException {		
		
		String conn = "jdbc:mysql://localhost/loja_virtual?userTimezone=true&serverTimezone=UTC";
		String user = "root";
		String pass = "root";
		
		return DriverManager.getConnection(conn, user, pass);
	}

}
