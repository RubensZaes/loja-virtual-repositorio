import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TestaConexao {

	public static void main(String[] args) throws SQLException {
		
		ConnectioFactory connectioFactory = new ConnectioFactory();
		Connection connection = connectioFactory.recuperarConexao();
		
		System.out.println("Connection Close!");
		
		connection.close();
		
	}

}
