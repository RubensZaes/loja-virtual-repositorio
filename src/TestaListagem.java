import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestaListagem {

	public static void main(String[] args) throws SQLException {

		ConnectioFactory connectioFactory = new ConnectioFactory();
		Connection connection = connectioFactory.recuperarConexao();

//		System.out.println("Connection Close!");

		Statement stm = connection.createStatement();
		stm.execute("SELECT id, nome, descricao FROM produto");
		ResultSet rst = stm.getResultSet();

		while (rst.next()) {
//			Integer id = rst.getInt(0); 
			Integer id = rst.getInt("ID");
//			System.out.println(id);

//			String nome = rst.getString(1);
			String nome = rst.getString("NOME");
//			System.out.println(nome);

//			String descricao = rst.getString(2);
			String descricao = rst.getString("DESCRICAO");
//			System.out.println(descricao);

			System.out.println("ID: " + id + " - NOME: " + nome + " - DESCRICAO: " + descricao);

		}

		connection.close();

	}

}
